My project: https://miebuch99.gitlab.io/aestheticprogramming/minix5/

<img src="HEADACHE.jpg" width="600">



**ABOUT THE PROJECT**

I choose to name this project "HEADACHE" because you can secure yourself a headache when observing the program run. The random colors and placement of circles captures your eyes and therefore also your attention. When following the circles move from line to line, you get sucked into the program and it will confuse your brain. You can say, that the program is like some sort of hypnosis, because it caughts your attention and your eyes will drawn from it. 

My thoughts about this program before starting it, was to use colors, because as in the 10 PRINT program and the Langton's Ant that we read about in the textbook, i thought it was a bit boring chose of colors. The change of color is not demanding, but i knew that it was something that my program should have loads of. 

**RULES OF THE PROGRAM**

Therefore i chose to use a random function both so the computer chooses the placement of the circles and their colors. For making the program run i used if and else, to make the computer draw elllipses when the conditional statement was true. You can see my code in the directory. 

**MEANING OF AUTO-GENERATOR**

I thinks it was funny to see how the computer could run by itself and choose random functions, like in mine; colors and placement of the circles. Every time you enter my link, the circles will be placed in a new position, so you wont get the same result every time. Or, i think at some point there must not be any more options and there will be result that are equal to eachother. 

**REFERENCE**

Video: https://www.youtube.com/watch?v=bEyTZ5ZZxZs by Daniel Shiffman.

Book: Soon, Winnie. Cox, Geoff. "A Handbook of Software Studies". 2020. p. 123-141.
