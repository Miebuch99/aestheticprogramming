let x = 0;
let y = 0;
let spacing = 15;

function setup() {
  createCanvas(1000, 650);
  background("#000000");
}

function draw() {

  if (random(1) < 0.5) {
    stroke(255);
    fill(random(255), 0, random(255));
    circle(x, spacing, spacing, y);
  } else {
    stroke(255);
    fill(random(225), 0, random(255));
    circle(x, y, spacing, spacing);
  }
  x = x + spacing;
  if (x > width) {
    x = 0;
    y = y + spacing;
  }
}
