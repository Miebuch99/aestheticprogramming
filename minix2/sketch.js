let moving_size = 70;
let static_size = 20;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(15);
}

function draw() {
  //Color of background is rosa
  background('#fae');

  //defining the color of the ellipse
  let c = color (255,204,0);
  fill (c);
  ellipse(300,250,300,300);

  //Eyes
  fill (600);
  circle (350,200,60,60);
  circle (250,200,60,60);

  //Eyecolor
  fill (150, 90, 700);
  circle (250,205,50,60);
  circle (350,205,50,60);

  fill (30);
  circle (250,210,30,30);
  circle (350,210,30,30);

  //mouth
  fill (30);
  ellipse(300,350, static_size, moving_size);

  //ellipse moves with the mouse and when it is pressed, the mouth of the smiley changes size
  ellipse(mouseX, mouseY, moving_size, moving_size);
  if(mouseIsPressed) {
    static_size = floor(random(100));
    }
}
