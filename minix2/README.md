My project: https://miebuch99.gitlab.io/aestheticprogramming/minix2/

<img src="abc1.jpg" width="600">


In this programme you can see a smiley. Since the last assignment i gave the smiley more details, by creating oter circles inside the eyes and giving it an eyecolor. Furthermore, the mouth has been changed, and when you press the mouse, the mouth will move. 

As mentioned in the first assignment, my experience in programming is almost non existing. This is also the reason why i am taking baby-steps in working on my programs. 

The emoji i made is very gender- and raceneutral, because the color of the skin is yellow, like the first emojis used to be. To be honest i choose this yellow color because i still dont know how to write a code that can make different skin-colors. It was also kind of hard for me to find the color of the eyes and mouth, but i just tried typing different numbers. 
It is clearly that there has not been many thoughts about gender, race or identity for making this emoji. I just tried making one with the codes that i knew from last assignment. I also tried using the code that was shown in the lecture earlier this week, to make the mouth move when mouse is pressed. 
I think it will be easier to incorporate these thoughts when my programming experience is wider. Because right now, my focus is just on writing the codes and trying to get them to work. 
