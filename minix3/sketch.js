function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(220);

  //guidelines(arrows) for using the loading-sign
  strokeWeight(5);
  line(60,180,60,240);
  line(205,20,230,40);
  line(205,60,230,40);
  line(230,40,160,40);
  line(40,200,60,180);
  line(80,200,60,180);
  line(350,180,350,240);
  line(350,240,330,220);
  line(350,240,370,220);
  line(160,350,230,350);
  line(180,330,160,350);
  line(180,370,160,350);

  //When the mouse hits different spots on the x and y axis, ellipses will be shown as a loading-sign
  if (mouseX > 180) {
    fill(0,162,223);
    ellipse(200,150,20,20);
  }

  if (mouseX > 200) {
    fill(0,162,223);
    ellipse(230,170,20,20);
  }
  if (mouseY > 200) {
    fill(143,188,230);
    ellipse(250,200,20,20);
  }
  if (mouseY > 230) {
    fill(143,188,230);
    ellipse(230,230,20,20);
  }
  if (mouseX < 200) {
    fill(206,225,244);
    ellipse(200,250,20,20);
  }
  if (mouseY < 250) {
    fill(206,225,244);
    ellipse(170,230,20,20);
  }
  if (mouseY < 230) {
    fill(226,234,252);
    ellipse(150,200,20,20);
  }
  if (mouseX > 150) {
    fill(226,234,252);
    ellipse(170,170,20,20);
  }
}
