**My project:** https://miebuch99.gitlab.io/aestheticprogramming/minix3/

<img src="abc2.jpg" width="600">

**About the project**

This assignment was for me to design a throbber using codes in JavaScript. This time I was a bit challenged, because in the previous assignments I “just” made smileys with no specific interaction, and therefore it is a big “jump” for me to make something interact. So you can say that this time also was for me to explore some more difficult codes and syntaxes.  

The process was hard for me, but I figured out a way that the throbber moves as the mouse. So when you turn your mouse in the way the arrows are showing you, there will be made kind of a throbber. 

With this throbber I want to express the meaning of time. The throbber will move in the speed that you turn your mouse around. YOU are then controlling the time. 
The thinking behind this throbber is therefore to express, that time is what you think it is. If you want time to be a stressfull factor, you will probably turn your mouse around very fast, but as you are in control of the speed of the throbber, it is also available for you to do this in a low speed. People are controlled by time, and in this project, I wanted to make the user in control of time. 

I used the syntax if (mouseX/Y </> 0) to make the circles in the throbber show when certain points on the x/y-axes were crossed. 

The throbber often communicates that the user have to wait for something. Therefore I made it possible in my assignment for the user, to interact with the throbber and thereby feel in control with time. At the same time it will be more positive to wait than negative. 
