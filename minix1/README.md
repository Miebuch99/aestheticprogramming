My project: https://miebuch99.gitlab.io/aestheticprogramming/minix1/

<img src="abc.jpg" width='600'>








The purpose of this assignment was to widen my horizon and learn the basic setup of coding in atom. With almost no experience in coding, it was a bit difficult, but throughout the process it became easier to understand the meaning of the different references used.

My first attempt ended up being a smiley. First of all, I wanted to change the background color, and therefore I used p5.js to find references to different color codes. Secondly, I wanted to do the smiley’s head. I used the reference "ellipse" here, but when I had to make the eyes afterwards, I chose to use the reference "circle" just to try another reference, even though it is the same form, but to create more opportunities for myself, and to create a greater understanding of the use of different codes. It was hard to find a suitable "mouth" for my smiley, but I ended up using the reference "arc".

It is clear that learning through experience is a perfect way for me to get to know how programming works. Throughout the process I changed the figures of the references so that the position of the eyes and the mouth was right. I also changed the numbers to get the right size of the different shapes. I was working on this project for several hours. I researched the different references, I was coding, making mistakes, trying to fix my mistakes etc. This is the way that I learn the best, by making mistakes, finding the problem and then fixing it. 

The process of coding and programming gives me the opportunity to be creative and to make mistakes. It is the best way to learn. For future assignments I will already know the codes that I used for this project, because I worked with them for hours, which gives me much more than just reading codes on a website and copying them. 
 
