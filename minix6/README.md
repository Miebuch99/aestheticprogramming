**MiniX6 - revisiting the past**

**Mood of corona lockdown**
- project: https://miebuch99.gitlab.io/aestheticprogramming/minix6/

**Which MiniX do you plan to rework?**

For this assignment i chose to rework minix4, because i had some very useful feedback that i wanted to incorporate in the project. My feedback was from Signe Regin Runge-Dalager and Sofie Juul Nisted. They discovered i had some trouble writing text in my project, so they suggested me to use the syntax text('', x, y). This was very helpful for me, because sometimes it helps to have some extra eyes looking on your project. 

**What have you changed and why?**

So the overall idea in this assignment was for me to write the text directly on the background, and not using a button which i did in the first place. You can see my minix4 on the screenshot down below: 

<img src="Screen3.jpg" width="600">

But i didn't think this was enough to change, so i also changed the color from greyscale to colorMode(HSB), which made it more aesthetically beauftiful to look at. One of the reasons why i wanted to change this was because i discovered that maybe it was a bit depressing that you only could choose "colors" in the greyscale, and therefore only choose sad colors that would visualize a bad mood. So the recreated minix is now giving you the upportunity to chose between more moods. 
I made a box with descriptions of the different colors and which mood they represents. This is for making it easier to use. 
I also wanted to use the if conditions statement, and therefore i made a button, that shows the color of my mood, when it is pressed. The color of my mood is green and will be presented as a stroke around the text. 
You can se the recreated minix down below:

<img src="Screen1.jpg" width="400">
<img src="Screen2.jpg" width="400">

**How would you demonstrate aesthetic programming in your work?**

I tried to make aesthetic programming appear in my work by making it more aesthetic to look at. You can, however, also argue for the greyscale to be aesthetic, because it visualizes lockdown as something negative, and between the lines it is showing the artists (my) feelings about lockdown. Or this is what anyone would think when interacting with the minix4 as it was. 
This was also the reason why i wanted to change the colors, and also make a button that would show my mood, so that there wouldnt be a misunderstanding on how i feel about lockdown: i am hopeful and sure that soon everything will be okay! :-) 

**What does it mean by programming as a practice, or even as a method for design?**

Using programming in making designs makes the thinking process long, because there are so many thoughts and rules behind writing the code. First of all you want to determine what the programme should contain, and therefore, as mentioned, there is also a lot of thinking behind it. 
It can, though, set limits on creativity, as there cant be faults in the codes because it wouldnt work. It demands you to do a lot of research on writing codes if you dont know it beforehand. 

**What is the relation between programming and digital culture?**

Programming makes it possible for us to interact on digital devices. Our culture has become more digital and the development is still continuing. As read in the text 'Variable geometry' (aesthetic programming, Soon, Cox), we use emojis to express our feelings, and the digital expressions has greater and greater impact on our everyday life. Even if using the emojis is a small thing, it is one between many thinks, that we might not think about, as being a digital element somehow having 'control' over our life. 

**Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?**

In this project as well as my previously projects, i had a lot of thougths behind the code and this is something mentioned in the text; that including reflective thinking is a big part of the process.

**REFERENCES**

**Text:** Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28.

**Book:** Aesthetic Programming. "A handbook of Software Studies" Soon, Winnie. Cox, Geoff. p. 53.

**Website:** https://p5js.org/reference/#/p5/createButton

**Website:** https://p5js.org/reference/#/p5/mouseIsPressed
