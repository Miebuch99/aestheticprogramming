let button;
let ctrackr;
let capture;
let slider;

function setup() {
  createCanvas(640, 480);
  background("#ff007c");

  colorMode(HSB);
  slider = createSlider(0, 255, [120], [0]);
  slider.position(270, 300);
  slider.style('width', '80px');

  let s = ('Slide to choose the color of your mood');

  button = createButton('Click here to see the color of my mood appear');
  button.position(190, 500);
  button.mousePressed(change);
}

function change() {
  if (mouseIsPressed) {
    stroke("#00ff00");
  }

}

function draw() {
  let val = slider.value();
  background(val, 100, 100, 100);

  textSize(20);
  text('Slide to choose the color of your mood in corona lockdown', 50, 50);

  textSize(14);
  text('Color guide', 60, 340);

  strokeWeight(5)
  rect(40, 320, 150, 190);

  textSize(12);
  text('Red: Angry', 60, 360);
  text('Orange: Calm', 60, 380);
  text('Yellow: Optimistic', 60, 400);
  text('Green: Hopeful', 60, 420);
  text('Turquoise: Peaceful', 60, 440);
  text('Blue: In control', 60, 460);


}
