let snake;
let rez = 20;
let food;
let w;
let h;
let bImg;
let sImg;
let cImg;
let song;
var button;

function MuteSong() {
  if (song.isPlaying()) {
    song.pause();
  } else {
    song.play();
  }
}

function preload() {
  soundFormats('mp3');
  song = loadSound('RobotUnicornAttackSong');
  bImg = loadImage('galaxy.jpg');
  sImg = loadImage('unicorn.png');
  cImg = loadImage('carrot.png');
}

function setup() {
  createCanvas(600, 600);
  song.play();
  button = createButton('Press for backgroundmusic!');
  button.position(10, 630);
  button.mousePressed(MuteSong);

  w = floor(width / rez);
  h = floor(height / rez);
  frameRate(5);
  snake = new Snake();
  foodLocation();

}

function foodLocation() {
  let x = floor(random(w));
  let y = floor(random(h));
  food = createVector(x, y);

}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    snake.setDir(-1, 0);
  } else if (keyCode === RIGHT_ARROW) {
    snake.setDir(1, 0);
  } else if (keyCode === DOWN_ARROW) {
    snake.setDir(0, 1);
  } else if (keyCode === UP_ARROW) {
    snake.setDir(0, -1);
  } else if (key == ' ') {
    snake.grow();
  }

}

function draw() {
  scale(rez);
  background(bImg);
  if (snake.eat(food)) {
    foodLocation();
  }
  snake.update();
  snake.show();


  if (snake.endGame()) {
    print("END GAME");
    background(255, 0, 0);
    noLoop();
  }


  noStroke();
  fill(255, 0, 0);
  image(cImg, food.x, food.y, 3, 3);
}
