- **Project:**  https://miebuch99.gitlab.io/aestheticprogramming/minix7/

**THE UNICORN MARCH**

<img src="TheUnicornMarch.jpg" width="600">

- **INTRODUCTION**
For this assignment i wanted to have a code to work from. So i downloaded the code for 'Daniel Shiffman - The Snake Game'. First i watched the videoes connected to the sample code, and tried to get an understanding of the meaning behind the different lines of code. 
To make the game my own, there was only one thing to do: "add a little spice". So i chose to change the snake into a unicorn, and change the background to something happier and more unicorn-friendly. I did also change the food to a carrot instead of a red rectangle. 

- **FUNCTIONALITY**
The game is actually not that complicated. It works just like the snake game we all know. You use the arros-keys to move the unicorn around, in the attempt to catch the carrot. When catching the carrot, another unicorn is added to the line, and the more you eat, the longer the line of unicorns will be. This is also the reason why i chose to call it "The Unicorn March" --> because an army of unicorns is created. 

- **DESCRIPTION OF THE OBJECTS IN THE PROGRAM**
There is created a class that defines the object (unicorn) - in the code it is defined as a snake. Then the attributes so as the size and position of the object is created, and also the function of making the object "grow" when eating the food is made. 
Personally i used a lot of time changing the background-, object- and food image in the syntax 'function preload' in my sketch.js. (You can look at my code in the repository). 
The background music is also created in the function preload. I also created a button to turn the music on and off.  


- **CHARACTERISTICS OF OBJECT-ORIENTED PROGRAMMING**
The characteristics of object oriented programming is to make objects with certain functions. When creating a class object, you design the object to look, act and be in a certain way, when - as in my game - catching carrots in a game etc. 
It also makes it a bit more manageable to go around, because you have you created object in one file, and the overall functions in another file. 

- **THE CULTURAL CONTEXT**
When thinking of the cultural aspects in my program, i came to the conclusion that i just wanted to make something everyone would think is funny. In these corona times we all need something to make us happy. The colors of my program and the chose of a unicorn as object illustrates this. 
Actually i cant think of an exact example, but the way i see it, all those details in making the objects and also coding is not that clear when you see the final object. You don't know how complex the objects actually are, and how many functions there is behind it. 

**REFERENCES**

- **VIDEOS**
- https://youtu.be/OMoVcohRgZA
- https://www.youtube.com/watch?v=jEwAMgcCgOA

- **MUSIC**
- Robot Unicorn Attack Song (Game from facebook)
