My projekt: https://miebuch99.gitlab.io/aestheticprogramming/minix4/

<img src="123.jpg" width="600">

Minix4

DEAL-BREAKER

I chose to give my projekt the title DEAL-BREAKER, because you can "show" how corona and the lockdown has affected your mood. Personally i think it can be hard to keep up the happy face, therefore the colors are a bit dark, and the scale is just going from black to white. If i would do anything different, i would make it possible to choose between every color. 
For the text i made a button, actually because i couldnt get it to work otherwise. So for next week i want to learn how to write text in the right way. Because by using a button it signifies that you can press it, and that it has a function. 

In this assignment i wanted to programme something to "collect data", but while doing this, i found out that it is much more complicated than i thought. As in the previous assignments, i am taking baby-steps in programming, because i have a really hard time writing these codes. 

I was trying to use the change function, so that the button could have a function. But most of the time i was trying to get the downloaded code to work, so i didn't have time for that. Therefore i just went with the slider, and gave that a function, so that when you slide, the background color changes.

I cant associate this projekt to capture all, or maybe a little. But the data (when you use the slider) wont be saved, so actually the data isnt captured. The data you give the computer when you use the slider, is anonymous, because you dont have to type your name or other information about yourself. 

Data capture can both be good and bad, because the information that you give can be saved forever. An example is pictures that you post on facebook, these will be on the internet forever, and you cant "take it back". But data capture on hospitals; journals, is for a good cause and will help doctors get important information about patients diseases. 

Reference p5.js code: https://p5js.org/reference/#/p5/createSlider
