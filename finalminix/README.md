_The final minix code has been handed in at Frederikke Breums gitlab account. _

**Link to Frederikkes gitlab account:** https://gitlab.com/frederikke.n.breum/aesthetic-programming/-/tree/master/ 

**Link to our project:** https://frederikke.n.breum.gitlab.io/aesthetic-programming/finalminix

<img src="finalminixsc1.jpg" width="600">

<img src="finalminixsc2.jpg" width="600">

**The Ideal World**

**What is the project about?**

Our program is going to highlight the difficulties for transgender people. We will via code illustrate the prejudices transgender people have met in their life. We will also illustrate how these people should be met in an ideal world. The setup consists of a black screen with statements in white. When you press the spacebar, the screen changes from one with prejudices to one with statements we think they should be met with instead. Therefore, there is no inbetween. We have drawn inspiration from two YouTube videos by Jamie Raines, also known as JammiDodger, where he, in one of the videos, talks about being transgender and in the other, his girlfriend reads comments he receives daily over the fact that he is transgender. We have decided to look at the comments in which he received in these two videos. 

**The source code in our program**

The following section will be based on Chapter 7 ‘Vocable Code’ in the book about Aesthetic Programming written by Soon, Winnie and Cox, Geoff.

Our code will be inspired by the artwork by Winnie, ‘Vocable code’, which emphasizes the significance of the source code and seeing it as a unified whole of its legible state and what is implemented in the user interface–rather than just a collection of instructions for functional reasons.

The artwork can be evaluated as a live-coding version that shows both the source code and what is executed, a user interface that only shows what is executed, and then only the source code. The statements appear very random in the sense that they shift up and down randomly in the center of the frame, fade out slowly, and are replaced by a new one.

The audio of various voices of the statements being echoed and disrupted continues to reinforce the noisy feel of the user interface. When the user is unfamiliar with the source code, the user is confused by the aesthetic features of the user interface, as the statements shift rapidly and unexpectedly.

While the voices are recorded of actual human voices, the fact that they are powered by a computer causes the audience to reflect on how human speech is transformed into something else when made by a computer program.
In the execution, the naturalness of the human voice is lost, and artificial ones are produced.
When working with and constructing the sense of the code, “we need to ‘sense’ the code to fully grasp what it is we are experiencing and to build an understanding of the code’s actions”.  (Cox, McLean and Ward)

When exploring structural and syntactic qualities, it is common practice to read the source code aloud in order to fully comprehend the code.
The programming code as a language must be designed in a way that combines natural language and computer behavior while detecting or attempting to understand it.

In our code we will attempt this by making the code understandable by combining natural language and computer language. It should be written in such a way that when users read it, they understand how the machine works.
Geoff Cox and Alex Mclean encourage readers to think of code as akin to poetry in the chapter "Vocable Code".

**Why the project is called ‘The Ideal World’**

We chose this title for the project because we wanted to express the consequenses of being yourself and living the life you want as a transgender. Because when changing your life in such an extreme way (like when changing your gender physically), you receive a lot of hate and discriminatory comments. Even though you are just trying to be yourself, people on the internet have thoughts and meanings about what is “wrong” and what is “right”. So as we mentioned above, the project is intended to point out the different prejudices that a transgender will receive, which also matches to the title, that says “The Ideal World”. The way that people have to be flawless to be accepted in the social culture, the project has to show that you are good enough just as you are. 

**Revised flowchart**

So when looking at our old flowchart we focused more on making a game and something fun and interactive, but after working with our final project draft we decided to make something with a deeper conceptual process behind it and focus on how transgender people are often perceived in todays society. 

<img src="screen1.jpg" width="600">

As it is a topic that has been discussed a lot in recent times, we have noticed that the general tone is very half and half - apparently either one can be very supportive and an ally or else be very harsh and transphobic. As a general observation on our part, it is becoming more common for people to add a “but” to their arguments, in order to not sound AS bias and stereotyping, e.g “I’m not racist, BUT…” or “I’m not transphobic, BUT …” and then finish the sentence by saying something equally racist and transphobic. Somehow by saying that one word, some people think it gives them an excuse to say something that is not normally accepted in today's society. That is one of the reasons why we wanted to put focus on the comments from two YouTube videos.

**About the chosen comments**

For the project we incorporated comments from two YouTube videos made by Jamie Raines (Jammidodges) where he, in the first one, shows some of his hate comments to his fiance. In the second video he shows comments that people have written about their prejudices against transgenders in general. In both videos Raines answers to the comments and tries to defend the lifestyle that he, and other transgenders have chosen to live. 
The reason that we chose these exact comments, is because we thought that the comments were really rude and expressed the exact atmosphere that you experience when being queer online. When we first read the comments, we were shocked about how people can talk to each other online, it’s like they forget that there are real people receiving those messages.

**What is Aesthetic programming, and how is it related to our work?**

In conjunction with the digital culture and its continuous development, we found it really important to use this exact platform to express our message. The comments we used for the project were found on YouTube, which also matches the idea of using a digital platform to perform our message about transgender. 

But an important question to ask ourselves is: ‘Why aesthetic programming is the right thing to use, when sending a message about transgender?’. We talked about this exact question, and came to the conclusion that aesthetic programming is about giving the code a deeper meaning. At the same time, it is able to give the reader of the code the opportunity to reflect upon and consider the code on a deeper level. This is also why we chose to use vocable code, because this gives the reader an opportunity to read between the lines, and to reflect on the things the code is telling them. In our code, which we also will explain down below, we tried to add some poetry to it, so the different lines of code gives the reader something to think about, and even something to reconsider in their own life as well. 
So an answer to that exact question about why aesthetic programming is the thing to use when trying to express the message about transgender, is actually several different answers. First of all, we have seen a lot of different videos on YouTube, where people explain and talk about their experiences with hate comments online, so in this point of view, we thought that giving an answer back, to those writing the hate comments, would be a nice idea to do on an interface that appeals to their natural environment. Secondly, we wanted the work to be visual and to express the message in a visual way, but without saying the direct words: ‘Treat people as you would like to be treated, and accept them just the way they are and what/who they want to be’, but instead “showing” it between the lines. Programming gave us an opportunity to be creative and make the work visual. Also, we managed to include the message in the critical questions that are asked when just looking at the work. The headline says “The Real World”, and “The Ideal World”, which asks a big question to the user/reader of the work, about the world being too real, and forgetting the people who are living in it. The Ideal World expresses how easy it is for us to be nice to each other, but even though it is this easy, a lot of people tend to send each other hate messages, forgetting that there are actual humans receiving these comments. This also underlines the huge development in the digital culture. We are living in a very digital world, our social life also lives on the digital media, even our private life is having it’s blast on the digital media. So, this explains why we chose to use programming in the hope of meeting these haters and giving them food for thought. Also, we want this to be a consideration about changing something in practice, so that we don’t forget the individuals behind the digital media, when using them for everything in our daily lives.

Using aesthetic programming also makes us understand programming on a wider political level. The growing importance of programming has to follow cultural development too. Overall, our work does not solve the problem of the hate towards transgender, but we are using programming to ask questions about it and to make people reconsider their behaviour towards each other online. 

The quote down below, also describes how aesthetic programming manages to produce immanent critique upon computer science, art, and cultural theory. This underlines the fact that we are trying to criticize and ask questions about the lack of morality in the digital culture. 
_“Aesthetic programming in this sense is considered as a practice to build things, and make worlds, but also produce immanent critique drawing upon computer science, art, and cultural theory.”_ (Soon & Cox, 2020, p. 15)

A crucial factor to talk about is also the language issue in programming. The computer controlled languages are formal, in contrast to the human language, which can be primitive and violent. In our work, we combined the use of computer language, and human language, when using the comments from the YouTube video, as well as the computer language syntaxes for writing the source code. But with the use of vocable code, we also manage to combine violent language with understandable, formal language.

The language in this work is something we have tried our best to sound as close to human language as possible, via the vocable code. Make the code sound like poetry, and something everybody could understand. However, we also wanted to be creative and test the boundaries of how we could critique the work itself. To make the programming language as esoteric as possible, by experimenting with weird ideas (like poetry) and the focus weren’t that the program and work needed to practical, rather than more reflective and a statement piece, however, we didn’t want the programming language to be as difficult to program/code in like most other ‘esolang’(https://esolangs.org/wiki/Main_Page). This is also something we will discuss further in the syntax, down below. 

**The syntax**

Through the source code we tried to include human language and not only computer language, to sort of give the code a say in the artwork that we have made. Because it is not only the interface that is our artwork, the source code is what builds it, and therefore we think that it has to support the message. We supported this by giving the titles of the different worlds a name that would leave the reader of the code wondering about how extreme the world and the people in it can affect those who are receiving the hate comments. The names of the different worlds are “The Real World”, and “The Ideal World”, so we chose to name the variables of the headline of  “The real world” ‘ignorance’, and named the headline on “The Ideal world” ‘sympathy’. By doing this, we are trying to frame the readers emotions, and make them wonder, and maybe even upscale the feelings themselves. 

Also, we chose to name the function of shifting between the real world and the ideal world “if the world was nice”, because this would make sense when putting it in an if conditional statement. When reading it, it says “If the world is nice, show the ideal world”. This also underlines that we want to emphasize the ideal world as the world we prefer, because in this world you can be yourself, and the only comments you get are “You are enough” and “You will always be enough”. In contrast to the real world, where the comments are a lot more offensive, the comments in the ideal world are also a booster to those reading our artwork, and a comment to them about them being good enough, how they are and what/who they want to be. 

By using vocable code and expressing the message through the source code, we think there is both good and bad. It can be confusing to understand that we want to say something through the code, because when you don’t know the code, you may not perceive that there is something to tell in the source code, other than it is telling the computer what to do. But the good thing is, when the reader is able to look at it, and read it aesthetically, there are a lot of comments from us, to the world, that the reader can think about and add a meaning to. Actually, you can say that the code is giving the reader an opportunity to reconsider how they behave, and how others behave against each other. 

**The poetry aspect of our work**

As described above, we wanted to incorporate a poetic aspect in our code. We did this by using vocable code, and making the code express something itself. In our source code, there is to find lines that says “If the world was nice, show the ideal world”, and the different variables are named ‘sympathy’, ‘ignorance’, ‘compassion’, and ‘humanity’ all with the purpose of giving the reader of the code something to consider and reflect on. 

**How would we queer code?**

As mentioned throughout this text, when focussing on Vocable Code, we find it important to think about how the program and the work can have a direct relation to how a human can resonate, interpret and reflect on said program - how computation can be so much more than just input and output. The goal for this work was to make up with this binary heterosexist paradigm of how people think it is okay to talk, or in this case write, in a demeaning and hateful way to someone different from themselves. As Soon and Cox mentions:   
“The notion of queer code is both the subject and the process of the work, and this operates on multiple levels, ‘queering’ what would be considered to be the normative conventions of software and its use: adressing what a front-end interface is expected to be, and how it performs normatively. What we experience are the performative qualities of code in terms of both its human and nonhuman execution.” (Soon & Cox, p. 168) 
By challenging the heterosexist gender norms and exposes the haters in our artwork, we uses queer code to put a spotlight on the frankly terrible rethoric used nowadays, as well as showing our support for transgender people and just people who are different in general.

**Appendix 1: Comments**

_Video 1: “Girlfriend responds to my haters”

“I’m not sure why people are afraid of trans people. They don’t exist. You are either a man or a woman from birth and it doesn’t change”

“I don't mind trans people and i think as people they may be interesting/nice, BUT i don't believe they are the gender they claim they think they are magically, the logic of that is flawed, and the maker of this video has flawed logic, you can't be transphobic if you don't dislike them. Just having the belief if only 2 genders doesn't make you a transphobe, sorry you are very wrong”

“Pick up a biology book”

“You are a woman.. blessings”

“IM THE FIRST  DISLIKE LETS GO!”

“How come you sound like a character of a real genuine person? It's almost like you are pretending to be someone…..like an actor playing a role. It feels like I am watching a caricature. Oh look, a cat. Exit stage left”

““Science” Sorry a couple of humanities papers with no citations except the few that cite other humanities papers aren’t science it’s just bigotry”

Video 2: “That’s not how trans works”

“Sigh. You’re an idiot, sir. Not all women ovulate, but only women ovulate. Not all women can have children, but only women can have children. Yes i know the difference btwn sex and gender but i suspect you don’t”

“Transgendered men do not become women, nor do transgenered women become men. All (including caitlyn Jenner) become feminized”

“Saying there are no aliens, is so very ignorant and sad. Yes they Believe In Unicorns, and transgenders., and they think two homosexuals are married! what a joke, what a laugh!”

“Cis women generally don’t have hair on their arms. If you know women who do, they are most likely trans or have some kind of hormone imbalance” 

“If trans f-m want to give away their wombs and ovaries to m-f trans we should make it law that they get consent from their mothers and grandmothers first...since those eggs were theirs before hers and still their descendants” 

“Trans men are men (but transwomen are not women)”_


**References:**

Esolang, the esoteric programming languages wiki (2005, April). Esoteric Programming Languages. Located May 12th 2021 at: https://esolangs.org/wiki/Main_Page 

Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press, 2013), 17-38			
			
Cox, Geoff, Alex McLean and Adrian Ward. The Aesthetics of Generative Code. 2001. Accessed 19 May 2020.
<http://www.generativeart.com/on/cic/2000/ADEWARD.HTM>.
	
Raines, Jamie. Jammidodger “Girlfriend Responds to my Haters”. YouTube. Accessed on May 4th 2021 on https://www.youtube.com/watch?v=7yEQoc1mgok  

Raines, Jamie. Jammidodger “That’s Not How Trans Works”. YouTube. Accessed on May 4th 2021 on https://www.youtube.com/watch?v=HnzCRlUE1do 

Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020.

